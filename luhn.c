#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){
	char *line = NULL;
 	size_t size;
	while (getline(&line, &size, stdin)!=-1) {
		line[strlen(line)-1]=0; // Strip trailing newline
		printf("%s%d\n", line, luhn(line));
	}
	return 0;
}

int luhn(const char* cc)
{

	const int m[] = {0,2,4,6,8,1,3,5,7,9};
	int i, sum = 0;
		 
 	for (i = strlen(cc); i--; i<=0) {
		int digit = cc[i] - '0';
		sum += i%2 ? m[digit] : digit;
	}
					 
	return (10-(sum % 10)) % 10;
}
