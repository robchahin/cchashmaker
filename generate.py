import argparse, hashlib
import warnings
from passlib.hash import bcrypt
from random import choice, randint

card_prefix = {
        'amex': ('34', '37'),
        'visa': ('4'),
        'mc':   ('51','52','53','54','55'),
        'discover': ('36','38'),
        'jcb': ('35')
}

parser = argparse.ArgumentParser(
        description="Luhn-valid credit card number generator.")
parser.add_argument(
        '-n', '--num', dest='numcards', action='store',
        help='number of cards to generate', type=int, default=1)
parser.add_argument(
        '-c', '--card-type', dest='card_type', action='store',
        help='types of card to generate', nargs='*', default='amex')
parser.add_argument(
    '-t', '--hash', dest='hash_type', action='store',
    help='type of hash to generate', nargs='?', default='sha128')

#TODO - choices option is really ugly, so handle card types manually


args = parser.parse_args()

def generate_card(card_type):
        prefix = choice(card_prefix[card_type])
        card = prefix
        num_digits = 15 - (1 if card_type == "amex" else 0)
        card+=''.join(
                ["%s" % randint(0,9) for num in range(
                        0, num_digits-len(prefix))
                ])
        card+=luhn_checksum(card)
        return card

def hash_card(card, hash_type):
        if hash_type == 'bcrypt':
            with warnings.catch_warnings():
                warnings.simplefilter("ignore") 
                return bcrypt.encrypt(card, salt="A6LhY2QnPAfu0Wny34p.0e")
        elif hash_type=='sha256':
            hashed_card = hashlib.sha256()
            hashed_card.update(card.encode('UTF-8'))
            return hashed_card.hexdigest()
        else:
            return card
def luhn_checksum(card_number):
        # 1. Count backwards, adding all numbers,
        #       doubling all 'odd' numbers, and keeping only num mod 10
        # 2. Return delta required to make total number 0 mod 10

        def digits_of(n):
                return [int(d) for d in str(n)]

        digits = digits_of(card_number)
        odd_digits = digits[-1::-2]
        even_digits = digits[-2::-2]

        checksum = sum(even_digits)

        for d in odd_digits:
                checksum += sum(digits_of(d*2))

        return  str((10-digits_of(checksum)[-1]) % 10)

for i in range(0,args.numcards):
    card_type = choice(args.card_type)
    card = generate_card(card_type)
    card = hash_card(card, args.hash_type)
    print (card)

#TODO add output options and arguments
#TODO add option for printing hashing instead of numbers

